package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class MeetingInvite {

    private List<TeamMember> requiredParticipants;
    private LocalDateTime deadLineForMeeting;

    public MeetingInvite(List<TeamMember> requiredParticipants, LocalDateTime deadLineForMeeting) {
        this.requiredParticipants = requiredParticipants;
        this.deadLineForMeeting = deadLineForMeeting;
    }


    boolean canMeet() {
        List<TeamMember> availableParticipants = this.requiredParticipants
                .stream()
                .filter(p -> p.canMeetBefore(deadLineForMeeting))
                .collect(toList());

        return availableParticipants.size() == this.requiredParticipants.size();
    }

    public String scheduleMeeting() {
        List<LocalDateTime> freeSlots = new ArrayList<>();
        if (!this.canMeet()) {
            return "Can not meet.";
        }

        for (int i = 0; i < requiredParticipants.size(); i++) {
            if (freeSlots.isEmpty()) {
                freeSlots.addAll(requiredParticipants.get(i).freeSlots());
                continue;
            }
            freeSlots.retainAll(requiredParticipants.get(i).freeSlots());
        }

        return freeSlots.get(0).format(DateTimeFormatter.ofPattern("EEEE dd, HH:mm a"));

    }
}
