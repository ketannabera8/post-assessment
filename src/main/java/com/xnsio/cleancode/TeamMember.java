package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.stream.Collectors.toList;

public class TeamMember {

    private String name;
    private List<LocalDateTime> scheduledMeetings;

    public TeamMember(String name, List<LocalDateTime> scheduledMeetings) {
        this.name = name;
        this.scheduledMeetings = scheduledMeetings;
        this.scheduledMeetings.sort(Comparator.naturalOrder());
    }

    public boolean canMeetBefore(LocalDateTime deadLineForMeeting) {
        return noMeetingsScheduled()
                || meetingCanBeScheduledBeforeAllTheMeetings(deadLineForMeeting)
                || meetingCanBeScheduledAfterAllMeetings(deadLineForMeeting)
                || meetingCanBeScheduledBetweenTwoMeetings(deadLineForMeeting);

    }

    private boolean meetingCanBeScheduledBetweenTwoMeetings(LocalDateTime deadLineForMeeting) {
        for (int i = 0; i < this.scheduledMeetings.size() - 1; i++) {
            LocalDateTime scheduledMeeting = this.scheduledMeetings.get(i);
            LocalDateTime scheduledMeetingEndTime = scheduledMeeting.plus(1, HOURS);
            LocalDateTime endTimeOfDesiredMeeting = deadLineForMeeting.plus(1, HOURS);
            LocalDateTime nextMeeting = scheduledMeetings.get(i + 1);
            if (scheduledMeetingEndTime.isBefore(deadLineForMeeting) && endTimeOfDesiredMeeting.isBefore(nextMeeting)) {
                return true;
            }
        }
        return false;
    }

    private boolean meetingCanBeScheduledAfterAllMeetings(LocalDateTime deadLineForMeeting) {
        LocalDateTime endTimeOfLastMeeting = this.scheduledMeetings.get(this.scheduledMeetings.size() - 1).plus(1, HOURS);
        return endTimeOfLastMeeting.isBefore(deadLineForMeeting) || endTimeOfLastMeeting.isEqual(deadLineForMeeting);
    }

    private boolean meetingCanBeScheduledBeforeAllTheMeetings(LocalDateTime deadLineForMeeting) {
        LocalDateTime firstMeeting = this.scheduledMeetings.get(0);
        LocalDateTime endTimeOfRequiredMeeting = deadLineForMeeting.plus(1, HOURS);
        return endTimeOfRequiredMeeting.isBefore(firstMeeting) || endTimeOfRequiredMeeting.isEqual(firstMeeting);
    }

    private boolean noMeetingsScheduled() {
        return this.scheduledMeetings.isEmpty();
    }

    public List<LocalDateTime> freeSlots() {
        if (scheduledMeetings.isEmpty()) {
            return new ArrayList<>();
        }
        List<LocalDateTime> freeSlots = new ArrayList<>();
        addFreeSlotsFromStartOfDayToFirstMeeting(freeSlots);
        addFreeSlotsFromLastMeetingTillEndOfDay(freeSlots);
        addFreeSlotsInBetweenMeetings(freeSlots);


        return freeSlots;
    }

    private void addFreeSlotsInBetweenMeetings(List<LocalDateTime> freeSlots) {
        for (int i = 0; i < this.scheduledMeetings.size() - 1; i++) {
            LocalDateTime meetingEndTime = scheduledMeetings.get(i).plus(1, HOURS);
            LocalDateTime nextMeeting = scheduledMeetings.get(i + 1);

            if (meetingEndTime.until(nextMeeting, HOURS) >= 1) {
                freeSlots.add(LocalDateTime.of(meetingEndTime.getYear(), meetingEndTime.getMonth(), meetingEndTime.getDayOfMonth(), meetingEndTime.getHour(), meetingEndTime.getMinute(), meetingEndTime.getSecond()));
            }
        }
    }

    private void addFreeSlotsFromLastMeetingTillEndOfDay(List<LocalDateTime> freeSlots) {
        LocalDateTime lastMeeting = this.scheduledMeetings.get(scheduledMeetings.size() - 1);
        LocalDateTime endOfDay = LocalDateTime.of(lastMeeting.getYear(), lastMeeting.getMonth(), lastMeeting.getDayOfMonth(), 17, 0, 0);
        addSlotsBetweenTwoTimes(freeSlots, lastMeeting.plus(1, HOURS), endOfDay);
    }

    private void addFreeSlotsFromStartOfDayToFirstMeeting(List<LocalDateTime> freeSlots) {
        LocalDateTime firstMeeting = this.scheduledMeetings.get(0);
        LocalDateTime startOfDay = LocalDateTime.of(firstMeeting.getYear(), firstMeeting.getMonth(), firstMeeting.getDayOfMonth(), 8, 0, 0);
        addSlotsBetweenTwoTimes(freeSlots, startOfDay, firstMeeting);
    }

    private void addSlotsBetweenTwoTimes(List<LocalDateTime> freeSlots, LocalDateTime startTime, LocalDateTime endTime) {
        if (startTime.until(endTime, HOURS) >= 1) {
            int hoursTillTheEndTime = (int) startTime.until(endTime, HOURS);
            for (int i = 0; i < hoursTillTheEndTime; i++) {

                freeSlots.addAll(Stream.iterate(startTime, d -> d.plusMinutes(1)).limit(MINUTES.between(startTime, startTime.plus(i, HOURS)) + 1).collect(toList()));
            }
        }
    }
}
