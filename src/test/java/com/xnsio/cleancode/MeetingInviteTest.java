package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDateTime;

import static java.util.Arrays.asList;
import static java.util.Collections.EMPTY_LIST;
import static org.junit.Assert.assertEquals;

public class MeetingInviteTest {

    TeamMember MINI_MAUSI = new TeamMember("Mini Mausi", EMPTY_LIST);
    TeamMember FRANK = new TeamMember("Frank", asList(LocalDateTime.of(2019, 11, 01, 10, 0)));
    TeamMember BRAD = new TeamMember("Brad", asList(LocalDateTime.of(2019, 11, 01, 8, 0)
            , LocalDateTime.of(2019, 11, 01, 9, 30)
            , LocalDateTime.of(2019, 11, 01, 10, 30)));

    TeamMember JANET = new TeamMember("Janet", asList(LocalDateTime.of(2019, 11, 01, 8, 0),
            LocalDateTime.of(2019, 11, 01, 9, 0),
            LocalDateTime.of(2019, 11, 01, 10, 0),
            LocalDateTime.of(2019, 11, 01, 11, 0),
            LocalDateTime.of(2019, 11, 01, 12, 0),
            LocalDateTime.of(2019, 11, 01, 13, 0),
            LocalDateTime.of(2019, 11, 01, 14, 15)));

    @Test
    public void miniMausiCanMeetFrankBefore5PMOnFriday1Nov() {
        LocalDateTime friday1Nov5PM = LocalDateTime.of(2019, 11, 01, 17, 0);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, FRANK), friday1Nov5PM);
        assertEquals("Friday 01, 08:00 AM", meetingInvite.scheduleMeeting());
    }

    @Test
    public void miniMausiCanMeetFrankBefore9AMOnFriday1Nov() {
        LocalDateTime friday1Nov9AM = LocalDateTime.of(2019, 11, 01, 9, 0);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, FRANK), friday1Nov9AM);
        assertEquals("Friday 01, 08:00 AM", meetingInvite.scheduleMeeting());
    }


    @Test
    public void miniMausiCanNotMeetBradBefore11AMOnFriday1Nov() {
        LocalDateTime friday1Nov11AM = LocalDateTime.of(2019, 11, 01, 11, 0);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, BRAD), friday1Nov11AM);
        assertEquals("Can not meet.", meetingInvite.scheduleMeeting());
    }

    @Test
    public void miniMausiCanNotMeetJanetBefore1PMOnFriday1Nov() {
        LocalDateTime friday1Nov1PM = LocalDateTime.of(2019, 11, 01, 13, 0);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, JANET), friday1Nov1PM);
        assertEquals("Can not meet.", meetingInvite.scheduleMeeting());
    }

    @Test
    public void miniMausiCanNotMeetFrankAndBradBeeBefore11AMOnFriday1Nov() {
        LocalDateTime friday1Nov11AM = LocalDateTime.of(2019, 11, 01, 11, 0);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, FRANK, BRAD), friday1Nov11AM);
        assertEquals("Can not meet.", meetingInvite.scheduleMeeting());
    }

    @Test
    public void frankAndBradCanMeetAt1130OnFriday1Nov() {
        LocalDateTime friday1Nov1130AM = LocalDateTime.of(2019, 11, 01, 11, 30);
        MeetingInvite meetingInvite = new MeetingInvite(asList(FRANK, BRAD), friday1Nov1130AM);
        assertEquals("Friday 01, 11:30 AM", meetingInvite.scheduleMeeting());
    }

    @Test
    public void miniMausiCanMeetFrankAndBradBefore2PMOnFriday1Nov() {
        LocalDateTime friday1Nov2PM = LocalDateTime.of(2019, 11, 01, 14, 00);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, FRANK, BRAD), friday1Nov2PM);
        assertEquals("Friday 01, 11:30 AM", meetingInvite.scheduleMeeting());
    }

    @Test
    public void miniMausiCanMeetFrankAndBradAndJanetBefore4PMOnFriday1Nov() {
        LocalDateTime friday1Nov2PM = LocalDateTime.of(2019, 11, 01, 16, 00);
        MeetingInvite meetingInvite = new MeetingInvite(asList(MINI_MAUSI, FRANK, BRAD, JANET), friday1Nov2PM);
        assertEquals("Friday 01, 15:15 PM", meetingInvite.scheduleMeeting());
    }
}